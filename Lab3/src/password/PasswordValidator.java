package password;

/*
 * 
 * @author C.Jack Jozwik - 991281499
 * 
 * Validates passwords are atleast 8 characters in length 
 * and contains atleast 2 digits
 * using TDD
 * 
 * */

public class PasswordValidator {

	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS = 2;
	
	public static boolean containsDigits(String password) {
		int digits = 0;
		
		for(int i = 0; i < password.length(); i++) {
			if (Character.isDigit(password.charAt(i))) {
				digits++;
			}
		}
		if (digits >= MIN_DIGITS) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param password
	 * @return true if number of chars is 8 or more. No spaces are allowed.
	 */
	public static boolean isValidLength(String password) {
		
		//if first first is false, doesnt go to second
		return password.indexOf(" ") < 0 && password.length() >= MIN_LENGTH;
		
	}
	
}
