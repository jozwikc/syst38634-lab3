
package test;

import static org.junit.Assert.*;

import org.junit.Test;

import password.PasswordValidator;

/*
 * 
 * @author C.Jack Jozwik - 991281499
 * JUnit testing for PasswordValidator Class
 * 
 * */

public class ValidatorTest {

	@Test
	public void testIsValidLengthRegular() {
		boolean result = PasswordValidator.isValidLength("1234567890");
		assertTrue("Invalid length", result);
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean result = PasswordValidator.isValidLength("");
		assertFalse("Invalid length", result);
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		boolean result = PasswordValidator.isValidLength("   t e s t       ");
		assertFalse("Invalid length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result = PasswordValidator.isValidLength("12345678");
		assertTrue("Invalid length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result = PasswordValidator.isValidLength("1234567");
		assertFalse("Invalid length", result);
	}
	
	@Test
	public void testContainsDigitsRegular() {
		boolean result = PasswordValidator.containsDigits("test12345");
		assertTrue("Invalid amount of digits", result);
	}
	
	@Test
	public void testContainsDigitsException() {
		boolean result = PasswordValidator.containsDigits("test");
		assertFalse("Invalid amount of digits", result);
	}
	
	@Test
	public void testContainsDigitsBoundaryIn() {
		boolean result = PasswordValidator.containsDigits("test12");
		assertTrue("Invalid amount of digits", result);
	}
	
	@Test
	public void testContainsDigitsBoundaryOut() {
		boolean result = PasswordValidator.containsDigits("test1");
		assertFalse("Invalid amount of digits", result);
	}

}
